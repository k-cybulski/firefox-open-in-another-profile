#!/usr/bin/env python3
# cheap remake of https://github.com/darktrojan/openwith/blob/master/webextension/native/open_with_linux.py
# with bits of https://github.com/mdn/webextensions-examples/tree/main/native-messaging

import os
import sys
import json
import struct
import subprocess

EXTENSION_NAME = "open_in_another_profile"
ALLOWED_EXTENSION_NAME = f"open_in_another_profile@kcyb.eu"
DEBUG_LOG = "/tmp/oiap.log"

def get_message():
    raw_length = sys.stdin.buffer.read(4)
    if len(raw_length) == 0:
        sys.exit(0)
    message_length = struct.unpack('@I', raw_length)[0]
    message = sys.stdin.buffer.read(message_length).decode('utf-8')
    return json.loads(message)

def validate_message(message):
    if not isinstance(message, list):
        return False
    if len(message) != 2:
        return False
    return True

def command_from_message(message):
    return ["firefox", "-P"] + message

# Send an encoded message to stdout
def send_message(message_content):
    encoded_content = json.dumps(message_content).encode('utf-8')
    encoded_length = struct.pack('@I', len(encoded_content))

    sys.stdout.buffer.write(encoded_length)
    sys.stdout.buffer.write(encoded_content)
    sys.stdout.buffer.flush()

def install():
    home_path = os.getenv('HOME')

    manifest = {
        'name': EXTENSION_NAME,
        'description': 'Plugin to open all links in another Firefox profile',
        'path': os.path.realpath(__file__),
        'allowed_extensions': [ALLOWED_EXTENSION_NAME],
        'type': 'stdio',
    }
    filename = f'{EXTENSION_NAME}.json'

    location = os.path.join(home_path, '.mozilla', 'native-messaging-hosts')
    if os.path.exists(os.path.dirname(location)):
        if not os.path.exists(location):
            os.mkdir(location)

    with open(os.path.join(location, filename), 'w') as file:
        file.write(
            json.dumps(manifest, indent=2, separators=(',', ': '), sort_keys=True).replace('  ', '\t') + '\n'
        )


def listen():
    received_message = get_message()
    with open(DEBUG_LOG, "a") as file_:
        file_.write(f"msg: {json.dumps(received_message)}\n")

    try:
        # Validate
        assert validate_message(received_message)
        command = command_from_message(received_message)

        devnull = open(os.devnull, 'w')
        subprocess.Popen(command, stdout=devnull, stderr=devnull)
        send_message(None)
    except Exception as e:
        with open(DEBUG_LOG, "a") as file_:
            file_.write(f"err: {e}\n")

if __name__ == '__main__':
    if len(sys.argv) == 2:
        if sys.argv[1] == 'install':
            install()
            sys.exit(0)

    if ALLOWED_EXTENSION_NAME in sys.argv:
        with open(DEBUG_LOG, "a") as file_:
            file_.write(f"argv: {json.dumps(sys.argv)}\n")
        listen()
        sys.exit(0)
