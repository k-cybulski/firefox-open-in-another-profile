/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * Sets up event listener to intercept requests.
 *
 * @async
 */
function instantiate() {
  // Remove previous listener
  browser.webRequest.onBeforeRequest.removeListener(intercept_request);
  browser.webNavigation.onCreatedNavigationTarget.removeListener(intercept_navigate);

  // Add listener!
  // in an ideal world, we would update the target profile here according to
  // configuration set in the interface, but in the world we are now this is
  // just hardcoded [TODO]
  browser.webRequest.onBeforeRequest.addListener(intercept_request, {
    urls: ["<all_urls>"]
  }, ["blocking"]);
  browser.webNavigation.onCreatedNavigationTarget.addListener(intercept_navigate);
}

/**
 * Check if URL should be opened in current profile.
 */
function is_url_local(url, permitted_domains) {
  let split = url.split("/");
  url_domain = split[2];

  for (let i = 0; i < permitted_domains.length; i++) {
    if (url_domain === permitted_domains[i]) {
      return true;
    }
  }
  return false;
}

/**
 * Is this URL permitted to be opened in current profile?
 *
 * If it would open a whole new tab, e.g. a link click, then check if the
 * domain is permitted. If it's not a whole new tab, it's probably just a thing
 * necessary to keep whatever web app we are on running.
 */
function is_request_for_current_profile(requestDetails) {

  // Is this e.g. a link click? Would this open in a whole tab?
  if (requestDetails.type == "main_frame") {
    return is_url_local(requestDetails.url, currentProfileDomains);
  } else {
    return true;
  }
}

/**
 * Callback for whenever a link is clicked, called from webRequest.onBeforeRequest [1]
 *
 * Sometimes this is called at the same time as the intercept_navigation
 * callback. Sometimes only one of them is called. So, there is a 100ms lock on
 * a URL after it has been opened, just in case both of them run.
 *
 * Cancels the request, and instead forwards it to the python executable which
 * calls the desired Firefox profile.
 *
 * [1] https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webRequest/onBeforeRequest
 */
function intercept_request(requestDetails) {
  if (logging) {console.log("Request details"); console.log(requestDetails);}
  if (is_request_for_current_profile(requestDetails)) {
    if (logging) {console.log("It's a local request");}
    return {};
  } else {
    open_in_profile(targetProfile, requestDetails.url);
    return {
      cancel: true
    };
  }
}

/**
 * Callback for navigation, called from webRequest.onBeforeNavigate [1].
 *
 * Sometimes this is called at the same time as the intercept_request callback.
 * Sometimes only one of them is called. So, there is a 100ms lock on a URL
 * after it has been opened, just in case both of them run.
 *
 * Closes tab if new tab opened, forwards the URL it to the python executable
 * which calls the desired Firefox profile.
 *
 * [1] browser.webNavigation.onBeforeNavigate.addListener
 */
async function intercept_navigate(navigationDetails) {
  if (logging) {console.log("Navigation detils"); console.log(navigationDetails); }
  open_in_profile(targetProfile, navigationDetails.url);
  browser.tabs.remove(navigationDetails.tabId)
  return {};
}

/**
 * Locks a URL for 500ms to prevent it opening twice, if both of the callbacks
 * wind up trying to open it.
 */
function lock_url(url) {
  urlLock[url] = true;
  setTimeout(function(){
    delete(urlLock[url]);
    if (logging) {console.log("Unlocked"); console.log(url);}
  }, 500);
}

function is_locked(url) {
  return Boolean(urlLock[url])
}

/**
 * Open the url in target profile
 */
function open_in_profile(profile, url) {
  // Command will be a list given to the native app.
  // It will have two elements:
  // - chosen firefox profile: for now a global constant
  // - the URL
  if (is_locked(url)) {
    if (logging) {console.log("Locked URL, skipping");}
    return;
  }
  lock_url(url);

  let command = [targetProfile];
  command.push(url);

  function error_listener(error) {
    console.error(error, browser.runtime.lastError);
  }
  let port = browser.runtime.connectNative('open_in_another_profile');
  port.onDisconnect.addListener(error_listener);
  port.onMessage.addListener(function() {
    port.onDisconnect.removeListener(error_listener);
    port.disconnect();
  });
  port.postMessage(command);
}

// The profile to open links in
const targetProfile = 'default-release';
// Patterns of sites which are permitted to run in current profile
const currentProfileDomains = [
  "web.whatsapp.com",
  "whatsapp.com"
];
const logging = false;
// A lock to prevent opening a link twice in quick succession, when
let urlLock = {};
instantiate();
