# Firefox plugin to open links in another window

This is a Firefox plugin which opens all links you click in another Firefox profile in another window, preventing new tabs from opening, with exception of a small list of domains that are allowed to open. The purpose of this plugin is to make it possible to run a Firefox window as a dedicated WhatsApp window, so that all links open up in another browser. By design, it's just a really minimal version of [darktrojan/openwith](https://github.com/darktrojan/openwith).

This plugin only works on Firefox on Linux. I wrote it to keep it dead simple, coding wise. At the moment, all of the important config details are hardcoded in `src/background.js`.

I don't plan to expand this plugin, other than to fix bugs (sneaky tabs opening when they shouldn't).

## Action plan

- Use [Native Messaging API](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging) to call `firefox -P <desired profile> <URL>` whenever a link is clicked
    - See examples: [MDN example plugin](https://github.com/mdn/webextensions-examples/tree/main/native-messaging)
- To catch whenever a link is clicked, use [browserAction.onClicked](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/browserAction/onClicked)
    - [webNavigation](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webNavigation)
- Prevent opening of new tabs using `webNavigation` and `tabs` API

## Building

This plugin can be built with `web-ext`. `cd` into the `src` directory, and run
```bash
web-ext build
```
This will create a happy little zipfile in the `web-ext-artifacts` directory.

## Installing

The happy little zipfile from the building step above can be installed in Firefox Developer Edition, with `xpinstall.signatures.required` set to `false` in `about:config`, [see here](https://support.mozilla.org/en-US/kb/add-on-signing-in-firefox?as=u&utm_source=inproduct).

It is necessary to run the executable
```
./src/runner/linux.py install
```
which will put a manifest JSON file enabling Native Messaging.
